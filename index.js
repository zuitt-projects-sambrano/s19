// alert("hi")

// ES6 Updates
	// EcmaScript6 or ES6 is the new version of javascript
	// Javascript is formally known as EcmaScript
	// ECMA- European Computer Manufacturers Association Script
		// ECMAScript standard defines the rules, details, and guidelines that the scripting language must observe to be considered ECMAScript compliant. 

/*
	Mini-Activity
	>> Create a new sentence string out of the strings that are provided using template literals

	>> Research the use of Math.pow() and show the result of 5 to the power of 3

	>> Send your browser console screenshot in our batch hangouts
*/

let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "javascript";

// Template Literals are part of JS ES6 Updates
	// ``- backticks
	// ${}-placeholder
let sentence = `${string1} ${string2} ${string3} ${string4} ${string5}`
console.log(sentence);

	// Concatenating strings with +
let phrase = "Hello" + " " + "World"
console.log(phrase);

// Exponent Operator (**)- ES6 update
	let fivePowerOf2 = 5 ** 2
	console.log(fivePowerOf2);
		// result: 25

// Using Math.pow(base, exponent)
let fivePowerOf3 = Math.pow(5, 3);
console.log(fivePowerOf3);
	// result: 125

// Template Literals with JS Expression
let sentence2 = `The result of five to the power of 2 is ${5**2}`
console.log(sentence2);

// Array Destructuring- this will allow us to save array items in a variable

let array = ["Kobe", "Lebron", "Shaq", "Westbrook"];

console.log(array[2])
	// result: Shaq

let lakerPlayer1 = array[3];
let lakerPlayer2 = array[0];

// Array Destructuring
const [kobe, lebron, shaq] = array;
// const [, , ,westbrook] = array;
	// assigning only to westbrook

console.log(kobe);
	// result: Kobe
console.log(lebron);
	// result: Lebron
console.log(shaq);
	// result: Shaq

let array2 = ["Curry", "Lillard", "Paul", "Irving"]
/*
	Mini Activity
	Save the items in the array in the following variables

	pointGuard1- Curry
	pointGuard2- Lillard
	pointGuard3- Paul
	pointGuard4- Irving

	Log the 4 variables in the console and use array destructuring. Send your browser screenshot in hangouts.

*/

const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = array2
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

let bandMembers = ["Hayley", "Zac", "Jeremy", "Taylor"];

const [vocals, lead,,bass] = bandMembers;
console.log(vocals);
console.log(lead);
console.log(bass);

// note: order matters in array destructuring
		// you can skip an item by adding another separator(,) but no variable name
// syntax: const/let [var1, var2] = array;

//Object Destructuring
	//this will allows us to destructure an object by allowing us to add the values of an object's property into respective variables

let person = {
	name: "Jeremy Davis",
	birthday: "September 12, 1989",
	age: 32
};

let sentence3 = `Hi I am ${person.name}`;
console.log(sentence3);

const {age, firstName, birthday} = person;

console.log(age);
	// result: 32
console.log(firstName);
	// result: undefined
		// person.firstName is not a property of person object.
console.log(birthday);
	// result: September 12, 1989

// note: Order does not matter in object destructuring

let pokemon1 = {
	name: "Charmander",
	level: 11,
	type: "Fire",
	moves: ["Ember", "Scratch", "Leer"]
};
/*
	Mini-Activity
	Create a new sentence variable with the following string/message:
		>> My pokemon is <pokemon1Name>, it is in level <pokemon1level>. It is a <typeOfPokemon>. It's moves are <pokemonMoves>

	Use Object destructuring
	Send your output in our batch hangouts

*/

const {name, level, type, moves} = pokemon1;
let sentence4 = `My pokemon is ${name}, it is in level ${level}. It is a ${type} type. It's moves are ${moves}`
console.log(sentence4);

// Arrow Functions
	// Arrow functions are alternative way of writing functions. However, arrow functions have significant pros and cons against the use of traditional function

// traditional function

function displayMsg(){
	console.log("Hello World!");
};

// arrow function
const hello = () => {
	console.log("Hello World Again!");
};

displayMsg();
hello();

const greet = (person) => {
	console.log(`Hi ${person.name}`)
};

greet(person);

// Implicit Return
	// allows us to return a value without the use of return keyword. 
	// implicit return only works with a one-liner function

const addNum = (num1, num2) => num1 + num2;
let sum = addNum(55, 60);
console.log(sum);

// this keyword

let protagonist = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
	// traditional method would have this keyword refer to the parent object
	greet: function(){
		console.log(this);
		console.log(`Hi I am ${this.name}`)
	},
	// arrow function refer to the global window
	introduce: () => {
		console.log(this);
		console.log(`I work as ${this.occupation}`)
	}
};
protagonist.greet();
protagonist.introduce();

// Class-Base Objects Blueprints
	// In Javascript, classes are templates of objects.
	// We can create objects out of the use of classes.
	// Before the introduction of Classes in JS, we mimic this behavior or this ability to create objects out of templates with the use of constructor functions


function Pokemon(name, type, lvl){
	this.name = name;
	this.type = type;
	this.lvl = lvl;
};

// ES6 Class Creation

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
let car1 = new Car("Toyota", "Vios", "2002");
console.log(car1);
let car2 = new Car("Cooper", "Mini", "1969");
console.log(car2);